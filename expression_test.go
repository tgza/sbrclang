package sbrclang

import (
	"math"
	"testing"
)

func TestExpression_TestSingle(t *testing.T) {
	e := NewExpression()
	e.PushTerm(&Term{1, MultiUnit{}})

	term, err := e.Evaluate()
	et := Term{1, MultiUnit{}}
	checkEqualTerms(*term, et, err, t)
}

func TestExpression_TestOpError(t *testing.T) {
	e := NewExpression()
	e.PushTerm(&Term{1, MultiUnit{}})
	e.PushOperator(PLUS_OP)

	term, err := e.Evaluate()
	if err == nil {
		t.Errorf("Expected error and got nil")
	}
	if term != nil {
		t.Errorf("Expected nil term and got %v", term)
	}
}

func TestExpression_TestTermError(t *testing.T) {
	e := NewExpression()
	e.PushTerm(&Term{1, MultiUnit{}})
	e.PushTerm(&Term{2, MultiUnit{}})

	term, err := e.Evaluate()
	if err == nil {
		t.Errorf("Expected error and got nil")
	}
	if term != nil {
		t.Errorf("Expected nil term and got %v", term)
	}
}

func TestExpression_TestPlus(t *testing.T) {
	e := NewExpression()
	e.PushTerm(&Term{1, MultiUnit{}})
	e.PushOperator(PLUS_OP)
	e.PushTerm(&Term{2, MultiUnit{}})

	term, err := e.Evaluate()
	et := newTestTerm(3, "")
	checkEqualTerms(*term, et, err, t)
}

func TestExpression_TestMinus(t *testing.T) {
	e := NewExpression()
	e.PushTerm(&Term{1, MultiUnit{}})
	e.PushOperator(MINUS_OP)
	e.PushTerm(&Term{2, MultiUnit{}})

	term, err := e.Evaluate()
	et := newTestTerm(-1, "")
	checkEqualTerms(*term, et, err, t)
}

func TestExpression_TestMul(t *testing.T) {
	e := NewExpression()
	e.PushTerm(&Term{2, MultiUnit{}})
	e.PushOperator(MULTIPLY_OP)
	e.PushTerm(&Term{3, MultiUnit{}})
	e.PushOperator(PLUS_OP)
	e.PushTerm(&Term{4, MultiUnit{}})
	e.PushOperator(MULTIPLY_OP)
	e.PushTerm(&Term{5, MultiUnit{}})

	term, err := e.Evaluate()
	et := newTestTerm(26, "")
	checkEqualTerms(*term, et, err, t)
}

func TestExpression_TestDiv(t *testing.T) {
	e := NewExpression()
	e.PushTerm(&Term{4, MultiUnit{}})
	e.PushOperator(DIVIDE_OP)
	e.PushTerm(&Term{2, MultiUnit{}})

	term, err := e.Evaluate()
	et := newTestTerm(2, "")
	checkEqualTerms(*term, et, err, t)
}

func TestExpression_TestUnitAssignmentPrecedence(t *testing.T) {
	e := NewExpression()
	e.PushTerm(&Term{18, MultiUnit{}})
	e.PushOperator(UNIT_ASSIGN_OP)
	s := newTestTerm(1, "min")
	e.PushTerm(&s)
	e.PushOperator(IMPLICIT_OP)
	e.PushTerm(&Term{3.1, MultiUnit{}})
	e.PushOperator(UNIT_ASSIGN_OP)
	d := newTestTerm(1, "mi")
	e.PushTerm(&d)

	term, err := e.Evaluate()
	et := newTestTerm(1/(5*60+(45./60.)), "mi !min")
	checkNilError(err, t)
	if term.units.DebugString() != "mi min^-1" || math.Abs(term.v-et.v) > 1 {
		t.Errorf("Term mismatch: got %v expected %v", term.DebugString(), et.DebugString())
	}
}
