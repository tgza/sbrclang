package sbrclang

type Token int

const (
	ILLEGAL Token = iota
	EOF
	WS

	ARROW
	AT

	TIME
	NUMBER
	UNIT

	RPAREN
	LPAREN

	PLUS
	MINUS
	MULTIPLY
	DIVIDE
)
