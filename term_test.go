package sbrclang

import (
	"math"
	"reflect"
	"testing"
)

func newTestTerm(f float64, s string) Term {
	return Term{v: f, units: parseMultiUnit(s)}
}

func checkNilError(err error, t *testing.T) {
	if err != nil {
		t.Errorf("Unexpected error: %q", err)
	}
}

func checkEqualTerms(term, et Term, err error, t *testing.T) {
	checkNilError(err, t)
	if !reflect.DeepEqual(term, et) {
		t.Errorf("Term mismatch: got %v expected %v", term.DebugString(), et.DebugString())
	}
}

func termEquals(l, r *Term) bool {
	return reflect.DeepEqual(l.units, r.units) && math.Abs(l.v-r.v) < 0.1
}

func TestNewTimeTerm(t *testing.T) {
	bad_times := []string{
		"",
		"0:",
		"0:0:0:0",
		"0:a",
		"::::",
		"a"}
	for _, s := range bad_times {
		term, err := NewTimeTerm(s)
		if err == nil || term != nil {
			t.Errorf("Expected error but instead got: %v for [%v]", term, s)
		}
	}

	good_times := []struct {
		input string
		sec   float64
	}{
		{"1.001", 1.001},
		{"60", 60},
		{"123", 123},
		{"0:60", 60},
		{"00:00:60", 60},
		{"1:00", 60},
		{"0:1:00", 60},
		{"00:01:00", 60},
		{"01:00:00", 3600},
		{"1:1:1", 3661}}
	mu := MultiUnit{Unit{"s", false, int(TIME_DIM)}}
	for _, v := range good_times {
		term, err := NewTimeTerm(v.input)
		if err != nil {
			t.Errorf("Unexpected error: %q for [%v]", err, v.input)
		}

		et := Term{v: v.sec, units: mu}
		if !reflect.DeepEqual(*term, et) {
			t.Errorf("Term mismatch: got %v expected %v for [%v]", term.DebugString(), et.DebugString(), v.input)
		}
	}
}

func TestTermPrettyString(t *testing.T) {
	tests := []struct {
		v  float64
		s  string
		es string
	}{
		{0, "", "0"},
		{1.234, "", "1.23"},
		{12345678.0123, "", "12345678.01"},
		{0, "m !s", "0 m / s"},
		{0.123, "m !s", "0.12 m / s"},
		{0, "s", "0:00.00"},
		{0.123, "s", "0:00.12"},
		{61.123, "s", "1:01.12"},
		{3601.123, "s", "1:00:01.12"},
	}
	for _, test := range tests {
		term := newTestTerm(test.v, test.s)
		ps := term.PrettyString(false)
		if ps != test.es {
			t.Errorf("PrettyString mismatch: got %v expected %v for test %v", ps, test.es, test)
		}
	}
}

func TestNewNumberTerm(t *testing.T) {
	term, err := NewNumberTerm("123")
	et := newTestTerm(123, "")
	checkEqualTerms(*term, et, err, t)

	if term.IsUnit() {
		t.Errorf("Term should not be a unit: %v", term)
	}

	_, err = NewNumberTerm("_XXX")
	if err == nil {
		t.Errorf("Expected error for _XXX")
	}
}

func TestNewUnitTerm(t *testing.T) {
	term, err := NewUnitTerm("s")
	et := newTestTerm(1, "s")
	checkEqualTerms(*term, et, err, t)

	if !term.IsUnit() {
		t.Errorf("Term should be a unit: %v", term)
	}

	_, err = NewUnitTerm("_XXX")
	if err == nil {
		t.Errorf("Expected error for _XXX")
	}
}

type OpTestCase struct {
	lv float64
	ls string
	rv float64
	rs string
	ev float64
	es string
}

type OpFunc func(l, r *Term) (*Term, error)

func RunTests(tests []OpTestCase, f OpFunc, t *testing.T) {
	for _, test := range tests {
		l := newTestTerm(test.lv, test.ls)
		r := newTestTerm(test.rv, test.rs)
		et := newTestTerm(test.ev, test.es)
		term, err := f(&l, &r)
		if err != nil {
			t.Errorf("Unexpected error: %q for test %v", err, test)
			continue
		}
		if !reflect.DeepEqual(term.units, et.units) || math.Abs(term.v-et.v) > 0.1 {
			t.Errorf("Term mismatch: got %v expected %v for test %v", term.DebugString(), et.DebugString(), test)
		}
	}
}

func TestTermAddSubtract(t *testing.T) {
	tests := []OpTestCase{
		{30, "s", 1, "min", 1.5, "min"},
		{1, "min", 30, "s", 90, "s"},
	}
	f := func(l, r *Term) (*Term, error) {
		return l.Add(r)
	}
	RunTests(tests, f, t)
}

func TestTermMultiply(t *testing.T) {
	tests := []OpTestCase{
		{30, "s", 2, "", 60, "s"},
		{30, "s", 10, "!ft", 300, "s !ft"},
		{1, "min", 30, "!s", 1800, ""},
		{120, "s", 2, "!min", 4, ""},
		{4.44, "m !s", 1, "min", 266.4, "m"},
	}
	f := func(l, r *Term) (*Term, error) {
		return l.Multiply(r)
	}
	RunTests(tests, f, t)
}

func TestTermDivide(t *testing.T) {
	tests := []OpTestCase{
		{30, "s", 2, "", 15, "s"},
		{30, "!s", 10, "!ft", 3, "ft !s"},
		{1, "min", 30, "s", 2, ""},
	}
	f := func(l, r *Term) (*Term, error) {
		return l.Divide(r)
	}
	RunTests(tests, f, t)
}

func TestTermImplicitOp(t *testing.T) {
	tests := []OpTestCase{
		// Two times is add.
		{2, "min", 3, "s", 123, "s"},
		// Two distances is add.
		{100, "m", 3, "km", 3.1, "km"},
		// Time and distance is divide.
		{20, "s", 100, "m", 5, "m !s"},
		// Distance and time is also divide.
		{100, "m", 20, "s", 5, "m !s"},
		// Default is multiply.
		{2, "", 3, "", 6, ""},
	}
	f := func(l, r *Term) (*Term, error) {
		return l.ImplicitOp(r)
	}
	RunTests(tests, f, t)
}
