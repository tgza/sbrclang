package sbrclang

import (
	"strings"
	"testing"
)

func TestScanner_Scan(t *testing.T) {
	testcases := []struct {
		input string
		tok   Token
		lit   string
	}{
		{"", EOF, ""},
		{" ", WS, " "},
		{"   ", WS, "   "},
		{"->", ARROW, "->"},
		{"@", AT, "@"},

		{":0", TIME, ":0"},
		{"0:0", TIME, "0:0"},
		{":", TIME, ":"}, // X - Will fail in the parser.
		{"0:0:0", TIME, "0:0:0"},

		{"1", NUMBER, "1"},
		{"1234.5678", NUMBER, "1234.5678"},
		{".1", NUMBER, ".1"},
		{"0.1", NUMBER, "0.1"},
		{".", NUMBER, "."},   // X
		{"..", NUMBER, ".."}, // X
		{"5k", NUMBER, "5"},  // May want to remove.

		{"m", UNIT, "m"},
		{"m1", UNIT, "m"}, // May want to remove.
		{"s", UNIT, "s"},
		{"marathon", UNIT, "marathon"},

		{"(", LPAREN, "("},
		{")", RPAREN, ")"},

		{"+", PLUS, "+"},
		{"-", MINUS, "-"},
		{"*", MULTIPLY, "*"},
		{"/", DIVIDE, "/"},

		{"?", ILLEGAL, "?"},
	}
	for _, v := range testcases {
		s := NewScanner(strings.NewReader(v.input))
		tok, lit := s.Scan()
		if tok != v.tok {
			t.Errorf("Expected token %v (lit=[%v]), got: %v (lit=[%v]) %v", v.tok, v.lit, tok, lit, v)
		}
		if lit != v.lit {
			t.Errorf("Expected literal [%v] (tok=%v), got: %v (lit=[%v]) %v", v.lit, v.tok, tok, lit, v)
		}
	}
}

func TestScanner_ScanSequence(t *testing.T) {
	testcases := []struct {
		input string
		tok   []Token
	}{
		{"", []Token{EOF}},
		{"+/", []Token{PLUS, DIVIDE}},
		{"5k", []Token{NUMBER, UNIT}},
		{"1+1 -> m", []Token{NUMBER, PLUS, NUMBER, ARROW, UNIT}},
		{"3:00:00 marathon -> mi", []Token{TIME, UNIT, ARROW, UNIT}},
		{"20 min * 6:45 mi -> t @ 1", []Token{NUMBER, UNIT, MULTIPLY, TIME, UNIT, ARROW, UNIT, AT, NUMBER}},
	}
	for _, v := range testcases {
		s := NewScanner(strings.NewReader(v.input))
		for _, etok := range v.tok {
			tok, lit := s.Scan()
			if tok == WS {
				tok, lit = s.Scan()
			}
			if tok != etok {
				t.Errorf("Expected token %v, got: %v [%v] %v", etok, tok, lit, v)
			}
		}
		tok, lit := s.Scan()
		if tok != EOF {
			t.Errorf("Expected EOF, got: %v [%v]", tok, lit)
		}
	}
}
