// https://en.wikipedia.org/wiki/Shunting-yard_algorithm

package sbrclang

import (
	"bytes"
	"fmt"
)

var verbose = false

type Operator int

const (
	UNIT_ASSIGN_OP Operator = iota + 1
	IMPLICIT_OP
	MULTIPLY_OP
	DIVIDE_OP
	PLUS_OP
	MINUS_OP
)

var opMap = map[Operator]string{
	UNIT_ASSIGN_OP: "<-",
	IMPLICIT_OP:    "[]",
	MULTIPLY_OP:    "*",
	DIVIDE_OP:      "/",
	PLUS_OP:        "+",
	MINUS_OP:       "-",
}

type Expression struct {
	output []interface{}
	ops    []Operator
}

func NewExpression() *Expression {
	return &Expression{
		output: make([]interface{}, 0),
		ops:    make([]Operator, 0),
	}
}

func (e *Expression) PushTerm(term *Term) {
	e.output = append(e.output, term)
}

func (e *Expression) shouldPopOp(op Operator) bool {
	if len(e.ops) == 0 {
		return false
	}
	// Return whether top has greater precendence.
	return e.ops[len(e.ops)-1] < op
}

func (e *Expression) PushOperator(op Operator) {
	for e.shouldPopOp(op) {
		var op Operator
		op, e.ops = e.ops[len(e.ops)-1], e.ops[:len(e.ops)-1]
		e.output = append(e.output, op)
	}
	e.ops = append(e.ops, op)
}

func (e *Expression) DebugString() string {
	var buf bytes.Buffer
	for i, v := range e.output {
		if i != 0 {
			buf.WriteString(" ")
		}
		switch v.(type) {
		case Operator:
			buf.WriteString(fmt.Sprintf("%v", opMap[v.(Operator)]))
		case *Term:
			buf.WriteString("(")
			buf.WriteString(v.(*Term).DebugString())
			buf.WriteString(")")
		}
	}

	if len(e.ops) > 0 {
		buf.WriteString(" ops: ")
		for i, v := range e.ops {
			if i != 0 {
				buf.WriteString(" ")
			}
			buf.WriteString(fmt.Sprintf("%v", v))
		}
	}
	return buf.String()
}

func executeOp(l *Term, op Operator, r *Term) (*Term, error) {
	switch op {
	case UNIT_ASSIGN_OP:
		return l.Multiply(r)
	case IMPLICIT_OP:
		return l.ImplicitOp(r)
	case MULTIPLY_OP:
		return l.Multiply(r)
	case DIVIDE_OP:
		return l.Divide(r)
	case PLUS_OP:
		return l.Add(r)
	case MINUS_OP:
		return l.Subtract(r)
	}
	return nil, fmt.Errorf("Unhandled operator %q", op)
}

func (e *Expression) Evaluate() (*Term, error) {
	for len(e.ops) > 0 {
		var op Operator
		op, e.ops = e.ops[len(e.ops)-1], e.ops[:len(e.ops)-1]
		e.output = append(e.output, op)
	}

	debug_string := e.DebugString()
	if verbose {
		fmt.Printf("expression: %v\n", debug_string)
	}

	stack := make([]*Term, 0)
	for _, qentry := range e.output {
		switch qentry.(type) {
		case Operator:
			if len(stack) < 2 {
				return nil, fmt.Errorf("Not enough parameters: %s", debug_string)
			}

			t, err := executeOp(stack[len(stack)-2], qentry.(Operator), stack[len(stack)-1])
			if err != nil {
				return nil, err
			}
			stack = stack[:len(stack)-1]
			stack[len(stack)-1] = t
		case *Term:
			stack = append(stack, qentry.(*Term))
		}
		if verbose {
			fmt.Printf("qentry: %v stack %v\n", qentry, stack)
		}
	}
	if len(stack) == 0 {
		return nil, fmt.Errorf("No remaining parameters: %s", debug_string)
	}
	if len(stack) > 1 {
		return nil, fmt.Errorf("Too many remaining parameters: %s", debug_string)
	}
	return stack[0], nil
}
