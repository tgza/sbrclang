package sbrclang

import (
	"bytes"
	"fmt"
	"math"
	"strconv"
	"strings"
)

type Term struct {
	v     float64
	units MultiUnit
}

func NewTimeTerm(s string) (*Term, error) {
	parts := strings.Split(s, ":")
	if len(parts) > 3 {
		return nil, fmt.Errorf("Too many parts for a time: %s", s)
	}
	if len(parts) == 0 {
		return nil, fmt.Errorf("Not enough parts for a time: %s", s)
	}

	mu, _ := NewMultiUnit("s", false)
	f0, err := strconv.ParseFloat(parts[0], 64)
	if err != nil {
		return nil, err
	}
	if len(parts) == 3 {
		f1, err := strconv.ParseFloat(parts[1], 64)
		if err != nil {
			return nil, err
		}
		f2, err := strconv.ParseFloat(parts[2], 64)
		if err != nil {
			return nil, err
		}
		return &Term{f0*60*60 + f1*60 + f2, mu}, nil
	} else if len(parts) == 2 {
		f1, err := strconv.ParseFloat(parts[1], 64)
		if err != nil {
			return nil, err
		}
		return &Term{f0*60 + f1, mu}, nil
	}
	return &Term{f0, mu}, nil
}

func NewNumberTerm(s string) (*Term, error) {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return nil, err
	}
	return &Term{f, MultiUnit{}}, nil
}

func NewUnitTerm(s string) (*Term, error) {
	mu, err := NewMultiUnit(s, false)
	if err != nil {
		return nil, err
	}
	return &Term{1, mu}, nil
}

func (t *Term) IsUnit() bool {
	return t.v == 1 && len(t.units) == 1
}

func (t *Term) IsScalar() bool {
	return t.units.Len() == 0
}

func (t *Term) DebugString() string {
	su := t.units.DebugString()
	if len(su) > 0 {
		return fmt.Sprintf("%.2f %s", t.v, su)
	}
	return fmt.Sprintf("%.2f", t.v)
}

func (t *Term) PrettyString(always_pad bool) string {
	if len(t.units) == 1 && t.units[0].sdim == int(TIME_DIM) {
		sunit, _ := NewUnitTerm("s")
		tu, _ := t.ConvertTo(sunit)
		v := tu.v

		var buf bytes.Buffer
		has_hours := false
		has_mins := false
		if v > 3600 {
			has_hours = true
			has_mins = true
			hrs := int(v / 3600)
			buf.WriteString(fmt.Sprintf("%d:", hrs))
			v = v - float64(hrs*3600)
		}

		if v > 60 {
			has_mins = true
			mins := int(v / 60.0)
			v = v - float64(mins*60.0)
			// Fix weird issue where "7:00 mi -> mi" returns 6:60.00.
			if math.Abs(v-60) < 0.001 {
				mins = mins + 1
				v = 0
			}

			if has_hours {
				buf.WriteString(fmt.Sprintf("%02d:", mins))
			} else {
				buf.WriteString(fmt.Sprintf("%d:", mins))
			}
		} else if has_hours {
			buf.WriteString("00:")
		}

		if has_mins {
			buf.WriteString(fmt.Sprintf("%05.2f", v))
		} else {
			buf.WriteString(fmt.Sprintf("0:%05.2f", v))
		}
		return buf.String()
	}

	su := t.units.PrettyString()

	var s string
	if t.v == math.Round(t.v) && !always_pad {
		s = fmt.Sprintf("%d", int(t.v))
	} else {
		s = fmt.Sprintf("%.2f", t.v)
	}
	if len(su) > 0 {
		return s + " " + su
	}
	return s
}

func (l *Term) Add(r *Term) (*Term, error) {
	scalar, err := l.units.ConvertTo(r.units)
	if err != nil {
		return nil, err
	}
	return &Term{l.v*scalar + r.v, r.units}, nil
}

func (l *Term) Subtract(r *Term) (*Term, error) {
	nr := &Term{-r.v, r.units}
	return l.Add(nr)
}

func (l *Term) Multiply(r *Term) (*Term, error) {
	scalar, ounits, err := l.units.Multiply(r.units)
	if err != nil {
		return nil, err
	}
	return &Term{scalar * l.v * r.v, ounits}, nil
}

func (l *Term) Divide(r *Term) (*Term, error) {
	scalar, ounits, err := l.units.Multiply(r.units.Inverse())
	if err != nil {
		return nil, err
	}
	return &Term{scalar * l.v / r.v, ounits}, nil
}

func (l *Term) ImplicitOp(r *Term) (*Term, error) {
	is_time := func(t *Term) bool {
		return len(t.units) == 1 && t.units[0].sdim == int(TIME_DIM)
	}
	is_distance := func(t *Term) bool {
		return len(t.units) == 1 && t.units[0].sdim == int(DISTANCE_DIM)
	}

	// Expressions like '18 min 10 s' should add the terms.
	if (is_time(l) && is_time(r)) ||
		(is_distance(l) && is_distance(r)) {
		return l.Add(r)
	}
	// Expressions like '6:00 mi' should be a speed so switch and then
	// divide the terms.
	if is_time(l) && is_distance(r) {
		return r.Divide(l)
	}
	// Expressions like 'mi 6:00' should also be a speed.
	if is_distance(l) && is_time(r) {
		return l.Divide(r)
	}
	// Multiply the terms and if the user does not get the answer they
	// want, they can be explicit.
	return l.Multiply(r)
}

func (l *Term) ConvertTo(r *Term) (*Term, error) {
	scalar, err := l.units.ConvertTo(r.units)
	if err != nil {
		return nil, err
	}
	return &Term{l.v * scalar / r.v, r.units}, nil
}
