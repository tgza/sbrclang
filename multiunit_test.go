package sbrclang

import (
	"fmt"
	"math"
	"reflect"
	"sort"
	"strings"
	"testing"
)

func parseMultiUnit(s string) MultiUnit {
	if len(s) == 0 {
		return MultiUnit{}
	}
	mu := MultiUnit{}
	parts := strings.Split(s, " ")
	for _, p := range parts {
		inv := false
		if p[0] == '!' {
			inv = true
			p = p[1:]
		}
		u, err := NewUnit(p, inv)
		if err != nil {
			return MultiUnit{}
		}
		mu = append(mu, u)
	}
	return mu
}

func TestMultiUnit(t *testing.T) {
	m, _ := NewMultiUnit("s", false)
	em := parseMultiUnit("s")
	if !reflect.DeepEqual(m, em) {
		t.Errorf("Unexpected multiunit: %v Expected %v", m, em)
	}

	m = append(m, Unit{"s", false, int(TIME_DIM)})
	em = parseMultiUnit("s s")
	if !reflect.DeepEqual(m, em) {
		t.Errorf("Unexpected multiunit: %v Expected %v", m, em)
	}

	m = m.Inverse()
	em = parseMultiUnit("!s !s")
	if !reflect.DeepEqual(m, em) {
		t.Errorf("Unexpected multiunit: %v Expected %v", m, em)
	}
}

func TestMultiUnitSort(t *testing.T) {
	m := parseMultiUnit("!s m s !m")
	sort.Sort(m)

	em := parseMultiUnit("m s !s !m")
	if !reflect.DeepEqual(m, em) {
		t.Errorf("Unexpected multiunit: %v Expected %v", m, em)
	}
	if m.DebugString() != "m s s^-1 m^-1" {
		t.Errorf("Unexpected DebugString: %s", m.DebugString())
	}
}

func TestMultiUnitConvert(t *testing.T) {
	tests := []struct {
		l, r string
		s    float64
	}{
		{"", "", 1},
		{"s", "s", 1},
		{"min", "s", 60},
		{"s", "min", 0.0166},
		{"feet", "inches", 12},
		{"!inches", "!feet", 12},
		{"mi !hour", "feet !sec", 1.5},
	}
	for _, test := range tests {
		l := parseMultiUnit(test.l)
		r := parseMultiUnit(test.r)
		s, err := l.ConvertTo(r)
		if err != nil {
			t.Errorf("Unexpected error: got %v for test %v", err, test)
			continue
		}
		if math.Abs(s-test.s) > 0.1 {
			t.Errorf("Unexpected conversion: got %v expected %v for test %v", s, test.s, test)
		}
	}
}

func TestMultiUnitConvertErrors(t *testing.T) {
	tests := []struct {
		l, r string
		err  string
	}{
		{"s", "m s", "Number of units do not match"},
		{"s", "m", "Incompatible units"},
		{"s", "!s", "Incompatible units"},
	}
	for _, test := range tests {
		l := parseMultiUnit(test.l)
		r := parseMultiUnit(test.r)
		_, err := l.ConvertTo(r)
		if err == nil {
			t.Errorf("Expected error and got none for test %v", test)
			continue
		}
		s := fmt.Sprintf("%v", err)
		if !strings.Contains(s, test.err) {
			t.Errorf("Error test does not match: got [%v] and expected [%v] for test %v", s, test.err, test)
		}
	}
}

func TestMultiUnitMultiply(t *testing.T) {
	tests := []struct {
		l, r string
		s    float64
		mu   string
	}{
		{"", "", 1, ""},
		{"s", "s", 1, "s s"},
		{"s", "!s", 1, ""},
		{"s", "!min", 0.016, ""},
		{"min", "!s", 60, ""},
		{"mi !hour", "sec !feet", 1.466, ""},
	}
	for _, test := range tests {
		l := parseMultiUnit(test.l)
		r := parseMultiUnit(test.r)
		s, mu, err := l.Multiply(r)
		if err != nil {
			t.Errorf("Unexpected error: got %v for test %v", err, test)
			continue
		}
		emu := parseMultiUnit(test.mu)
		if math.Abs(s-test.s) > 0.1 || !reflect.DeepEqual(mu, emu) {
			t.Errorf("Convert mismatch: got %v,%v expected %v,%v for test %v", s, mu, test.s, emu, test)
		}
	}
}
