package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sbrclang"
	"strings"
	"text/tabwriter"
)

func main() {
	var p *sbrclang.Parser
	if len(os.Args) == 1 {
		p = sbrclang.NewParser(bufio.NewReader(os.Stdin))
	} else {
		p = sbrclang.NewParser(strings.NewReader(os.Args[1]))
	}
	result, err := p.Run()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(result.T.PrettyString(false))
	if len(result.It) > 0 {
		fmt.Println()
		const padding = 3
		w := tabwriter.NewWriter(os.Stdout, 0, 0, padding, ' ', tabwriter.AlignRight)
		for i, v := range result.It {
			fmt.Fprintf(w, "%02d\t%s\t%s\t\n", i+1, v.L.PrettyString(true), v.R.PrettyString(true))
		}
		w.Flush()
	}
}
