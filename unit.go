package sbrclang

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"
)

type Dimension int

const (
	UNKNOWN_DIM Dimension = iota
	TIME_DIM
	DISTANCE_DIM
)

// Unit is a named quantity such as 'meters', 'm', 's', 's^-1'.
type Unit struct {
	// Entry in nodeMap which returns ok in GetUnit().
	name string
	// If true, then this unit exists in the denominator.
	inv bool
	// Cached sortable Dimension that is negated if inv is true. Set if
	// instantiated using NewCompoundUnit. Used when combining units for
	// algebraic operations.
	sdim int
}

func NewUnit(name string, inv bool) (Unit, error) {
	if val, ok := GetUnit(name); ok {
		var u Unit
		if inv {
			u = Unit{name, inv, int(-val.dim)}
		} else {
			u = Unit{name, inv, int(val.dim)}
		}
		return u, nil
	}
	return Unit{}, fmt.Errorf("Unknown unit: %s", name)
}

func (u Unit) DebugString() string {
	if u.inv {
		return u.name + "^-1"
	}
	return u.name
}

func (u Unit) Inverse() Unit {
	return Unit{u.name, !u.inv, -u.sdim}
}

// Nodes in the unit conversion graph.
type UnitNode struct {
	name        string
	dim         Dimension
	conversions []UnitConversion
	edges       []Edge
}

func NewUnitNode(name string, dim Dimension) *UnitNode {
	return &UnitNode{
		name:        name,
		dim:         dim,
		conversions: make([]UnitConversion, 0),
		edges:       make([]Edge, 0)}
}

func (n *UnitNode) addEdge(c UnitConversion) {
	n.conversions = append(n.conversions, c)
	n.edges = append(n.edges, c)
}

func (n *UnitNode) Edges() []Edge {
	return n.edges
}

func (n *UnitNode) edgeTo(b *UnitNode) *UnitConversion {
	for _, e := range n.conversions {
		if e.ounit == b.name {
			return &e
		}
	}
	return nil
}

var nodeMap = map[string]*UnitNode{}

func GetUnit(name string) (*UnitNode, bool) {
	name = strings.ToLower(name)
	val, ok := nodeMap[name]
	if !ok && strings.HasSuffix(name, "s") {
		val, ok = nodeMap[name[:len(name)-1]]
	}
	return val, ok
}

// Edges in the unit conversion graph.
type UnitConversion struct {
	ounit  string
	scalar float64
	inv    bool
}

func (u UnitConversion) Destination() Node {
	return nodeMap[u.ounit]
}

func (u UnitConversion) isSame() bool {
	return u.scalar == 1
}

func (u UnitConversion) isPerfect() bool {
	_, frac := math.Modf(u.scalar)
	return frac == 0
}

func (u UnitConversion) Weight() float64 {
	if u.isSame() {
		return 0
	}
	if u.isPerfect() {
		return 1
	}
	return 2
}

func init() {
	conversions := []struct {
		names                 string
		ounit                 string
		conversion_expression string
	}{
		{"ft,feet,foot", "m", "* 0.3048"},
		{"in,inch,inches", "ft", "/ 12"},
		{"yard,yd", "ft", "* 3"},
		{"mi,mile", "ft", "* 5280"},
		{"furlong", "ft", "* 660"},

		{"meter", "m", "* 1"},
		{"cm,centimeter", "m", "/ 100"},
		{"km,k,kilometer", "m", "* 1000"},

		{"second,sec", "s", "* 1"},
		{"min,minute", "s", "* 60"},
		{"hr,hour", "min", "* 60"},
		{"d,day", "hr", "* 24"},

		{"marathon", "mi", "* 26.2"},
		{"halfmarathon", "mi", "* 13.1"},
		{"metricmile", "m", "* 1500"},

		{"GIMP", "mi", "* 31"},
		{"bridgeofdoom", "mi", "* 1.3"},
		{"parkloop", "mi", "* 3.3"},
		{"loopdebergen", "mi", "* 7.0"},
		{"carrollparkloop", "mi", "* 0.2769"},
		{"stride", "m", "* 100"},
	}

	nodeMap["s"] = NewUnitNode("s", TIME_DIM)
	nodeMap["m"] = NewUnitNode("m", DISTANCE_DIM)

	for _, c := range conversions {
		var inv bool
		if c.conversion_expression[0] == '/' {
			inv = true
		} else {
			inv = false
		}
		p := strings.Split(c.conversion_expression, " ")
		if len(p) != 2 {
			log.Fatalf("Expected 2 parts in conversion expression: %q - Got %q", c, p)
		}
		scalar, err := strconv.ParseFloat(p[1], 64)
		if err != nil {
			log.Fatalf("Could not parse conversion expression: %q. Got %q", c, err)
		}

		parts := strings.Split(c.names, ",")
		for i, part := range parts {
			var n *UnitNode
			if _, ok := nodeMap[part]; ok {
				log.Fatalf("Duplicate unit conversion: %q", part)
			} else {
				n = NewUnitNode(part, UNKNOWN_DIM)
				nodeMap[part] = n
			}

			if i == 0 {
				// Link the first listed unit to the other unit, e.g. ft -> m
				// and m -> ft.
				n.addEdge(UnitConversion{c.ounit, scalar, inv})
				nodeMap[c.ounit].addEdge(UnitConversion{part, scalar, !inv})
			} else {
				// Link synonyms to the base unit name, e.g. feet -> ft
				// instead of feet -> m.
				n.addEdge(UnitConversion{parts[0], 1, false})
				nodeMap[parts[0]].addEdge(UnitConversion{part, 1, false})
			}

			_, err := ShortestPath(nodeMap[part], nodeMap["s"])
			if err == nil {
				n.dim = TIME_DIM
			} else {
				_, err := ShortestPath(nodeMap[part], nodeMap["m"])
				if err == nil {
					n.dim = DISTANCE_DIM
				} else {
					log.Fatalf("Unknown unit type for %q", part)
				}
			}
		}
	}
}

func ConvertUnit(l, r string) (float64, error) {
	if l == r {
		return 1, nil
	}
	lu, ok := GetUnit(l)
	if !ok {
		return 0, fmt.Errorf("Unknown left unit: %s", l)
	}
	ru, ok := GetUnit(r)
	if !ok {
		return 0, fmt.Errorf("Unknown right unit: %s", r)
	}
	path, err := ShortestPath(lu, ru)
	if err != nil {
		return 0, err
	}

	f := float64(1)
	prev := lu
	for _, p := range path {
		cur := p.(*UnitNode)
		if prev.name == cur.name {
			prev = cur
			continue
		}

		e := prev.edgeTo(cur)
		if e == nil {
			return 0, fmt.Errorf("Could not find edge to %s to %s", prev.name, cur.name)
		}
		if e.inv {
			f = f / e.scalar
		} else {
			f = f * e.scalar
		}
		prev = cur
	}
	return f, nil
}
