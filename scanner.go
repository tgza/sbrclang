package sbrclang

import (
	"bufio"
	"bytes"
	"io"
)

func isWhitespace(ch rune) bool {
	return ch == ' ' || ch == '\t' || ch == '\n'
}

func isDigit(ch rune) bool {
	return (ch >= '0' && ch <= '9') || (ch == '.') || (ch == ':')
}

func isLetter(ch rune) bool {
	return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')
}

var eof = rune(0)

type Scanner struct {
	r   *bufio.Reader
	pos int
}

func NewScanner(r io.Reader) *Scanner {
	return &Scanner{r: bufio.NewReader(r)}
}

func (s *Scanner) scanWhitespace() (tok Token, lit string) {
	var buf bytes.Buffer
	for {
		ch := s.read()
		if !isWhitespace(ch) {
			s.unread(ch)
			break
		}
		buf.WriteRune(ch)
	}
	return WS, buf.String()
}

func (s *Scanner) scanUnit() (tok Token, lit string) {
	var buf bytes.Buffer
	for {
		ch := s.read()
		if !isLetter(ch) {
			// Can error out if ch is not a whitespace. That would prevent
			// 'm1' from being parsed as a unit.
			s.unread(ch)
			break
		}
		buf.WriteRune(ch)
	}
	return UNIT, buf.String()
}

func (s *Scanner) scanNumber() (tok Token, lit string) {
	var buf bytes.Buffer
	token_type := NUMBER
	for {
		ch := s.read()
		if !isDigit(ch) {
			// Can error out if ch is not a whitespace or ':'. That would
			// prevent '5k' from being parsed as a number.
			s.unread(ch)
			break
		} else {
			if ch == ':' {
				token_type = TIME
			}
			_, _ = buf.WriteRune(ch)
		}
	}
	return token_type, buf.String()
}

func (s *Scanner) read() rune {
	ch, _, err := s.r.ReadRune()
	if err != nil {
		return eof
	}
	s.pos++
	return ch
}

func (s *Scanner) unread(ch rune) {
	s.pos--
	_ = s.r.UnreadRune()
}

func (s *Scanner) Scan() (tok Token, lit string) {
	ch := s.read()
	if isWhitespace(ch) {
		s.unread(ch)
		return s.scanWhitespace()
	} else if isLetter(ch) {
		s.unread(ch)
		return s.scanUnit()
	} else if isDigit(ch) {
		s.unread(ch)
		return s.scanNumber()
	}

	switch ch {
	case eof:
		return EOF, ""
	case '(':
		return LPAREN, string(ch)
	case ')':
		return RPAREN, string(ch)
	case '+':
		return PLUS, string(ch)
	case '-':
		nch := s.read()
		if nch == '>' {
			return ARROW, "->"
		}
		s.unread(nch)
		return MINUS, string(ch)
	case '*':
		return MULTIPLY, string(ch)
	case '/':
		return DIVIDE, string(ch)
	case '@':
		return AT, string(ch)
	}

	return ILLEGAL, string(ch)
}
