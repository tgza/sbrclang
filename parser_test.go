package sbrclang

import (
	"fmt"
	"strings"
	"testing"
)

func toSeconds(s string) float64 {
	t, _ := NewTimeTerm(s)
	return t.v
}

type ParserTestCase struct {
	s  string
	ev float64
	es string
}

func TestParserLeftOnly(t *testing.T) {
	tests := []ParserTestCase{
		{"1 + 2 * 3", 7, ""},
		{"2 * (3 + 4)", 14, ""},
		{"(((2 * (((3 + 4))))))", 14, ""},
		{"2 * 3 + 4 * 5", 26, ""},
		{"60 * s", 60, "s"},
		{"60 * s + 10 * s", 70, "s"},
		{"60 * s + 1 * min", 2, "min"},
		{"0:10 + 1:00", 70, "s"},
		{"6:00 mi", 0, "mi !s"},
	}
	for _, test := range tests {
		p := NewParser(strings.NewReader(test.s))
		result, err := p.Run()
		if err != nil {
			t.Errorf("Unexpected error: %q for test %v", err, test)
			continue
		}
		et := newTestTerm(test.ev, test.es)
		if !termEquals(result.T, &et) {
			t.Errorf("Term mismatch: got %v expected %v for test %v", result.T.DebugString(), et.DebugString(), test)
		}
	}
}

func TestParserConversion(t *testing.T) {
	tests := []ParserTestCase{
		{"10560 ft -> mi", 2, "mi"},
		{"5 km -> mi", 3.1, "mi"},
		{"5 km -> m", 5000, "m"},
		{"5 * 1 km -> 3 km", 1.67, "km"},
		{"marathon -> mi", 26.2, "mi"},
		{"3:00:00 marathon -> mi / min", 0.15, "mi !min"},
		{"3:00:00 marathon -> mi", toSeconds("6:52.2"), "s"},
		// Lap times for 6:00 mi.
		{"6:00 1600 m -> 400 m", 90, "s"},
		{"6:00 1600 m -> min", 266.67, "m"},
		// Remaining miles of a 9mi run that includes a V02 max workout
		// consisting of 7 reps of fast 3 min followed by 3 min cool down
		{"9 mi - 7 * (6:00 mi * 3 min + 9:00 mi * 3 min) -> mi", 3.1, "mi"},
		// Running a 5k at 5:45 pace will get what time.
		{"5:45 mi -> 5 km", toSeconds("17:51.8"), "s"},
		// The 1000s 5k requires what pace.
		{"1000 s 5 km -> mi", toSeconds("5:21.8"), "s"},
		{"(1000 s) (5 km) -> mi", toSeconds("5:21.8"), "s"},
		{"(1000 s) / (5 km) -> mi", toSeconds("5:21.8"), "s"},
		// General aerobic run for 30 min is how far.
		{"7:30 mi * 30 min -> mi", 4, "mi"},
		// General aerobic run for 5mi will take how long.
		{"7:30 mi -> 5 mi", toSeconds("37:30"), "s"},
		// General aerobic run is how many miles per hour.
		{"7:30 mi -> mi / hour", 8, "mi !hour"},
		// Bolt's speed in the world record.
		{"100 m / 9.58 s  -> mi / hour", 23.35, "mi !hour"},
		// Finish Western States in 24hr.
		{"100 mi / 24 hr -> mi", 0.24, "hr"},
	}
	for _, test := range tests {
		p := NewParser(strings.NewReader(test.s))
		result, err := p.Run()
		if err != nil {
			t.Errorf("Unexpected error: %q for test %v", err, test)
			continue
		}
		et := newTestTerm(test.ev, test.es)
		if !termEquals(result.T, &et) {
			t.Errorf("Term mismatch: got %v expected %v for test %v", result.T.DebugString(), et.DebugString(), test)
		}
	}
}

type ExpectedTerm struct {
	ev float64
	es string
}

type ExpectedTermPair struct{ l, r ExpectedTerm }

type IterateTestCase struct {
	s  string
	t  ExpectedTerm
	it []ExpectedTermPair
}

func (i *IterateTestCase) toResult() *Result {
	et := newTestTerm(i.t.ev, i.t.es)
	r := &Result{T: &et}
	if len(i.it) > 0 {
		for _, it := range i.it {
			el := newTestTerm(it.l.ev, it.l.es)
			er := newTestTerm(it.r.ev, it.r.es)
			r.It = append(r.It, TermPair{&el, &er})
		}
	}
	return r
}

func resultEquals(l, r *Result) bool {
	if !termEquals(l.T, r.T) {
		return false
	}
	if len(l.It) != len(r.It) {
		return false
	}
	for i := range l.It {
		if !termEquals(l.It[i].L, r.It[i].L) {
			return false
		}
		if !termEquals(l.It[i].R, r.It[i].R) {
			return false
		}
	}
	return true
}

func newExpectedTermPair(lv float64, ls string, rv float64, rs string) ExpectedTermPair {
	return ExpectedTermPair{ExpectedTerm{lv, ls}, ExpectedTerm{rv, rs}}
}

func TestParserIterate(t *testing.T) {
	tests := []IterateTestCase{
		{"7:30 mi -> mi @ 4",
			ExpectedTerm{toSeconds("7:30"), "s"},
			[]ExpectedTermPair{
				newExpectedTermPair(toSeconds("7:30"), "s", 1, ""),
				newExpectedTermPair(toSeconds("15:00"), "s", 2, ""),
				newExpectedTermPair(toSeconds("22:30"), "s", 3, ""),
				newExpectedTermPair(toSeconds("30:00"), "s", 4, ""),
			}},
		{"7:30 mi -> mi @ 4 mi",
			ExpectedTerm{toSeconds("7:30"), "s"},
			[]ExpectedTermPair{
				newExpectedTermPair(toSeconds("7:30"), "s", 1, "mi"),
				newExpectedTermPair(toSeconds("15:00"), "s", 2, "mi"),
				newExpectedTermPair(toSeconds("22:30"), "s", 3, "mi"),
				newExpectedTermPair(toSeconds("30:00"), "s", 4, "mi"),
			}},
		{"7:30 mi -> 7:30 @ 4",
			ExpectedTerm{1, "mi"},
			[]ExpectedTermPair{
				newExpectedTermPair(1, "mi", 1, ""),
				newExpectedTermPair(2, "mi", 2, ""),
				newExpectedTermPair(3, "mi", 3, ""),
				newExpectedTermPair(4, "mi", 4, ""),
			}},
		{"6:00 1600 m -> 400 m @ 1600 m",
			ExpectedTerm{90, "s"},
			[]ExpectedTermPair{
				newExpectedTermPair(90, "s", 400, "m"),
				newExpectedTermPair(180, "s", 800, "m"),
				newExpectedTermPair(270, "s", 1200, "m"),
				newExpectedTermPair(360, "s", 1600, "m"),
			}},
		{"18:00 5k -> mi @ 5 k",
			ExpectedTerm{toSeconds("5:47.6"), "s"},
			[]ExpectedTermPair{
				newExpectedTermPair(toSeconds("05:47.6"), "s", 1, "mi"),
				newExpectedTermPair(toSeconds("11:35.2"), "s", 2, "mi"),
				newExpectedTermPair(toSeconds("17:22.8"), "s", 3, "mi"),
				newExpectedTermPair(toSeconds("18:00.0"), "s", 3.11, "mi"),
			}},
		{"100 mi / 24 hr -> mi @ 2",
			ExpectedTerm{0.24, "hr"},
			[]ExpectedTermPair{
				newExpectedTermPair(0.24, "hr", 1, ""),
				newExpectedTermPair(0.48, "hr", 2, ""),
			}},
	}
	for _, test := range tests {
		p := NewParser(strings.NewReader(test.s))
		result, err := p.Run()
		if err != nil {
			t.Errorf("Unexpected error: %q for test %v", err, test)
			continue
		}
		eresult := test.toResult()
		if !resultEquals(result, eresult) {
			t.Errorf("Result mismatch: got %v expected %v for test %v", result.DebugString(), eresult.DebugString(), test.s)
		}
	}
}

func TestParserError(t *testing.T) {
	tests := []struct {
		s   string
		err string
	}{
		{"(60 ", "Did not find matching"},
		{"60 (*)", "Unknown token"},
		{"60 ) 2", "Expected '->'"},
		{"60 * ft + 1 * min", "Incompatible units"},
		{"60 -> ft / s", "Left expression has no units"},
		{"60 * ft -> 1", "Right expression has no units"},
		{"60 * ft * s * m -> ft", "Can only convert expressions with 2"},
		{"60 * ft * s -> ft", "can only convert fractions"},
		{"60 * ft * s -> ft @ 1 @", "Expected EOF"},
	}
	for _, test := range tests {
		p := NewParser(strings.NewReader(test.s))
		_, err := p.Run()
		if err == nil {
			t.Errorf("Expected error for test %v", test)
			continue
		}
		s := fmt.Sprintf("%v", err)
		if !strings.Contains(s, test.err) {
			t.Errorf("Error test does not match: got [%v] and expected [%v] for test %v", s, test.err, test)
		}
	}
}
