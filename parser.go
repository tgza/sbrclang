// Parses the grammar for sbrclang. See README.md for grammar details.
//
// References:
//
// https://blog.gopheracademy.com/advent-2014/parsers-lexers/
// https://github.com/benbjohnson/sql-parser
// https://blog.gopheracademy.com/advent-2017/parsing-with-antlr4-and-go/

package sbrclang

import (
	"fmt"
	"io"
	"math"
)

// Max number of iterations allowed when using the '@' operator.
var max_it_limit = 101

type TermPair struct{ L, R *Term }

type Result struct {
	T  *Term
	It []TermPair
}

func (r *Result) DebugString() string {
	if r.T == nil {
		return "(nil)"
	}
	s := r.T.DebugString()
	if len(r.It) > 0 {
		s = s + " it:  "
		for i, v := range r.It {
			if i != 0 {
				s = s + " : "
			}
			s = s + v.L.DebugString()
			if v.R != nil {
				s = s + " / " + v.R.DebugString()
			}
		}
	}
	return s
}

type Parser struct {
	s   *Scanner
	lhs *Expression
	rhs *Expression
	it  *Expression

	buf struct {
		tok Token
		lit string
		n   int
	}
}

func NewParser(r io.Reader) *Parser {
	return &Parser{s: NewScanner(r)}
}

func (p *Parser) scanOnce() (tok Token, lit string) {
	if p.buf.n != 0 {
		p.buf.n = 0
		return p.buf.tok, p.buf.lit
	}
	tok, lit = p.s.Scan()
	p.buf.tok, p.buf.lit = tok, lit
	return
}

func (p *Parser) scan() (tok Token, lit string) {
	tok, lit = p.scanOnce()
	if tok == WS {
		tok, lit = p.scanOnce()
	}
	return
}

func (p *Parser) unscan() { p.buf.n = 1 }

func (p *Parser) peek(t Token) bool {
	tok, _ := p.scan()
	p.unscan()
	return tok == t
}

func (p *Parser) parse(expected_token Token) bool {
	tok, _ := p.scan()
	if tok == expected_token {
		return true
	}
	p.unscan()
	return false
}

func (p *Parser) parseOperator() Operator {
	tok, _ := p.scan()
	if tok == PLUS {
		return PLUS_OP
	}
	if tok == MINUS {
		return MINUS_OP
	}
	if tok == DIVIDE {
		return DIVIDE_OP
	}
	if tok == MULTIPLY {
		return MULTIPLY_OP
	}
	p.unscan()
	return IMPLICIT_OP
}

func (p *Parser) parseTerm() (*Term, error) {
	tok, lit := p.scan()

	if tok == LPAREN {
		exp, err := p.parseExpression()
		if err != nil {
			return nil, err
		}
		tok, _ := p.scan()
		if tok != RPAREN {
			return nil, fmt.Errorf("Did not find matching left parenthesis at %v", p)
		}
		return exp.Evaluate()
	}
	if tok == TIME {
		return NewTimeTerm(lit)
	}
	if tok == NUMBER {
		return NewNumberTerm(lit)
	}
	if tok == UNIT {
		return NewUnitTerm(lit)
	}
	return nil, fmt.Errorf("Unknown token %q, %q at %v", tok, lit, p.s.pos)
}

func (p *Parser) parseExpression() (*Expression, error) {
	var err error

	exp := Expression{}
	if p.peek(ARROW) || p.peek(EOF) {
		return &exp, nil
	}

	lterm, err := p.parseTerm()
	if err != nil {
		return nil, err
	}
	if lterm == nil {
		return nil, fmt.Errorf("Expected term at %v", p)
	}
	exp.PushTerm(lterm)

	for {
		if p.peek(ARROW) || p.peek(EOF) || p.peek(RPAREN) || p.peek(AT) {
			return &exp, nil
		}

		op := p.parseOperator()
		rterm, err := p.parseTerm()
		if err != nil {
			return nil, err
		}
		if rterm == nil {
			return nil, fmt.Errorf("Expected rterm at %v", p)
		}

		if op == IMPLICIT_OP {
			// For expressions like '18:00 5 k' we want the unit assignment
			// of k to 5 to occur before the implicit operator. This is
			// handled by giving unit assignment higher precedence than the
			// implicit operator. Without this, the implicit operator would
			// not know that '5 k' is a distance and would multiply '18:00'
			// by 5.
			if lterm.IsScalar() && rterm.IsUnit() {
				exp.PushOperator(UNIT_ASSIGN_OP)
			} else {
				exp.PushOperator(IMPLICIT_OP)
			}
		} else {
			exp.PushOperator(op)
		}
		exp.PushTerm(rterm)

		lterm = rterm
	}
}

func (p *Parser) parseProgram() error {
	// Parse left.
	var err error
	p.lhs, err = p.parseExpression()
	if err != nil {
		return err
	}
	if ok := p.parse(EOF); ok {
		return nil
	}

	// Maybe parse right.
	if ok := p.parse(ARROW); !ok {
		tok, lit := p.scan()
		return fmt.Errorf("Expected '->' got %v, [%v]", tok, lit)
	}
	p.rhs, err = p.parseExpression()
	if err != nil {
		return err
	}
	if ok := p.parse(EOF); ok {
		return nil
	}

	// Maybe parse iteration.
	if ok := p.parse(AT); !ok {
		tok, lit := p.scan()
		return fmt.Errorf("Expected '@' got %v, [%v]", tok, lit)
	}
	p.it, err = p.parseExpression()
	if err != nil {
		return err
	}
	if ok := p.parse(EOF); ok {
		return nil
	}
	return fmt.Errorf("Expected EOF at %d", p.s.pos)
}

func solveForMissingDimension(missing_sdim int, l, r *Term) (*Term, error) {
	if missing_sdim < 0 {
		nt, err := l.Divide(r)
		if err != nil {
			return nil, err
		}
		nt.v = 1.0 / nt.v
		nt.units = nt.units.Inverse()
		return nt, nil
	}

	nt, err := l.Multiply(r)
	if err != nil {
		return nil, err
	}
	return nt, nil
}

func convert(l, r, it *Term) (*Result, error) {
	if l.units.Len() == 0 {
		return nil, fmt.Errorf("Left expression has no units")
	}
	if r.units.Len() == 0 {
		return nil, fmt.Errorf("Right expression has no units")
	}
	if l.units.Len() == r.units.Len() {
		if it != nil {
			return nil, fmt.Errorf("Can only iterate when solving for missing dimension")
		}
		nt, err := l.ConvertTo(r)
		return &Result{T: nt}, err
	}
	if r.units.Len() > l.units.Len() {
		l, r = r, l
	}
	if l.units.Len() != 2 {
		return nil, fmt.Errorf("Can only convert expressions with 2 or less units")
	}

	unit_total := 0
	for _, u := range l.units {
		if u.sdim > 0 {
			unit_total++
		} else {
			unit_total--
		}
	}
	if unit_total != 0 {
		return nil, fmt.Errorf("If there is more than one unit, can only convert fractions")
	}

	// Find the missing dimension.
	var mu Unit
	for _, lu := range l.units {
		found := false
		for _, ru := range r.units {
			if math.Abs(float64(lu.sdim)) == math.Abs(float64(ru.sdim)) {
				found = true
				break
			}
		}
		if !found {
			mu = lu
			break
		}
	}

	nt, err := solveForMissingDimension(mu.sdim, l, r)
	if err != nil {
		return nil, err
	}
	result := &Result{T: nt}
	if it == nil {
		return result, nil
	}

	result.It = make([]TermPair, 0)
	if it.IsScalar() {
		limit := int(it.v)
		if limit > max_it_limit {
			limit = max_it_limit
		}
		for i := 1; i <= limit; i++ {
			rc := Term{r.v * float64(i), r.units}
			nt, err := solveForMissingDimension(mu.sdim, l, &rc)
			if err != nil {
				return nil, err
			}
			index := Term{float64(i), MultiUnit{}}
			result.It = append(result.It, TermPair{L: nt, R: &index})
		}
	} else {
		upper, err := it.Divide(r)
		if err != nil {
			return nil, err
		}

		limit := 0
		for i := 1; i <= int(math.Ceil(upper.v)) && limit < max_it_limit; i++ {
			rc := Term{r.v * float64(i), r.units}
			e, err := rc.ConvertTo(&Term{1, it.units})
			if err != nil {
				return nil, err
			}
			if e.v > it.v {
				e = upper
				rc.v = r.v * float64(upper.v)
			}

			nt, err := solveForMissingDimension(mu.sdim, l, &rc)
			if err != nil {
				return nil, err
			}
			result.It = append(result.It, TermPair{L: nt, R: &rc})

			limit = limit + 1
		}
	}
	return result, nil
}

func (p *Parser) Run() (*Result, error) {
	err := p.parseProgram()
	if err != nil {
		return nil, err
	}
	if p.lhs == nil {
		return nil, nil
	}

	l, err := p.lhs.Evaluate()
	if err != nil {
		return nil, err
	}
	if p.rhs == nil {
		return &Result{T: l}, nil
	}

	r, err := p.rhs.Evaluate()
	if err != nil {
		return nil, err
	}

	var it *Term
	if p.it != nil {
		var err error
		it, err = p.it.Evaluate()
		if err != nil {
			return nil, err
		}
	}

	return convert(l, r, it)
}
