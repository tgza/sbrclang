package sbrclang

import (
	"bytes"
	"fmt"
	"sort"
)

type MultiUnit []Unit

func NewMultiUnit(name string, inv bool) (MultiUnit, error) {
	u, err := NewUnit(name, inv)
	if err != nil {
		return nil, err
	}
	return MultiUnit{u}, nil
}

func (m MultiUnit) Inverse() MultiUnit {
	r := make([]Unit, 0)
	for _, u := range m {
		r = append(r, u.Inverse())
	}
	return r
}

func (m MultiUnit) Len() int           { return len(m) }
func (m MultiUnit) Swap(i, j int)      { m[i], m[j] = m[j], m[i] }
func (m MultiUnit) Less(i, j int) bool { return m[i].sdim > m[j].sdim }

func (m MultiUnit) DebugString() string {
	var buf bytes.Buffer
	for i, v := range m {
		if i != 0 {
			buf.WriteString(" ")
		}
		buf.WriteString(v.DebugString())
	}
	return buf.String()
}

func (m MultiUnit) PrettyString() string {
	var buf bytes.Buffer

	inv := false
	for i, v := range m {
		if i != 0 {
			buf.WriteString(" ")
		}
		if inv == false && v.inv == true {
			inv = true
			buf.WriteString("/ ")
		}
		buf.WriteString(v.name)
	}
	return buf.String()
}

// Two MultiUnits can be added together if they are of the same
// length and each of their units are of the same dimension.
func (l MultiUnit) ConvertTo(r MultiUnit) (float64, error) {
	if l.Len() != r.Len() {
		return 0, fmt.Errorf("Number of units do not match: %d - %d", l.Len(), r.Len())
	}

	sort.Sort(l)
	sort.Sort(r)
	for i, _ := range l {
		if l[i].sdim != r[i].sdim {
			return 0, fmt.Errorf("Incompatible units: cannot convert %v to %v at index %d", l.DebugString(), r.DebugString(), i)
		}
	}

	v := float64(1)
	for i, _ := range l {
		scalar, err := ConvertUnit(l[i].name, r[i].name)
		if err != nil {
			return 0, err
		}
		if l[i].inv {
			v = v / scalar
		} else {
			v = v * scalar
		}
	}
	return v, nil
}

type fraction struct {
	top    MultiUnit
	bot    MultiUnit
	scalar float64
}

func (f *fraction) insert(u Unit) {
	if u.inv {
		f.bot = append(f.bot, u)
		return
	}
	f.top = append(f.top, u)
}

func newFraction(l, r MultiUnit) *fraction {
	f := fraction{}
	f.top = MultiUnit{}
	f.bot = MultiUnit{}
	f.scalar = 1

	for _, u := range l {
		f.insert(u)
	}
	for _, u := range r {
		f.insert(u)
	}
	return &f
}

func (f *fraction) simplifyOnce() (bool, error) {
	for i, t := range f.top {
		for j, b := range f.bot {
			if t.sdim == -b.sdim {
				scalar, err := ConvertUnit(t.name, b.name)
				if err != nil {
					return false, err
				}
				f.scalar = f.scalar * scalar
				f.top = append(f.top[:i], f.top[i+1:]...)
				f.bot = append(f.bot[:j], f.bot[j+1:]...)
				return true, nil
			}
		}
	}
	return false, nil
}

func (f *fraction) simplify() error {
	for {
		b, err := f.simplifyOnce()
		if err != nil {
			return err
		}
		if !b {
			break
		}
	}
	return nil
}

func (l MultiUnit) Multiply(r MultiUnit) (float64, MultiUnit, error) {
	f := newFraction(l, r)
	if err := f.simplify(); err != nil {
		return 0, nil, err
	}
	out := f.top
	out = append(out, f.bot...)
	sort.Sort(out)
	return f.scalar, out, nil
}
