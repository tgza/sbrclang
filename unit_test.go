package sbrclang

import (
	"fmt"
	"math"
	"reflect"
	"strings"
	"testing"
)

func TestInverse(t *testing.T) {
	u := Unit{"s", false, 1}
	ui := u.Inverse()
	eui := Unit{"s", true, -1}
	if !reflect.DeepEqual(ui, eui) {
		t.Errorf("Unexpected unit: %v Expected %v", ui, eui)
	}

	if u.DebugString() != "s" {
		t.Errorf("Unexpected debug string: %v", u.DebugString())
	}
	if ui.DebugString() != "s^-1" {
		t.Errorf("Unexpected debug string: %v", ui.DebugString())
	}
}

func TestGetUnit(t *testing.T) {
	tests := []struct {
		u    string
		name string
		dim  Dimension
	}{
		{"s", "s", TIME_DIM},
		{"mi", "mi", DISTANCE_DIM},
		{"MI", "mi", DISTANCE_DIM},      // Name gets converted to lower case.
		{"miles", "mile", DISTANCE_DIM}, // Plural works.
	}
	for _, test := range tests {
		s, ok := GetUnit(test.u)
		if !ok {
			t.Errorf("Unexpected error for test %v", test)
			continue
		}
		if s.name != test.name || s.dim != test.dim {
			t.Errorf("Unexpected unit: got %v for test %v", s, test)
		}
	}
	_, ok := GetUnit("_XXX")
	if ok {
		t.Errorf("Should not have gotten unit _XXX")
	}
}

func TestConvertUnit(t *testing.T) {
	tests := []struct {
		l, r string
		s    float64
	}{
		{"s", "s", 1},
		{"s", "min", 1 / 60.},
		{"min", "s", 60},
		{"hr", "s", 3600},
		{"furlong", "m", 201.168},
	}
	for _, test := range tests {
		s, err := ConvertUnit(test.l, test.r)
		if err != nil {
			t.Errorf("Unexpected error: %q for test %v", err, test)
			continue
		}
		if math.Abs(s-test.s) > 0.1 {
			t.Errorf("Scalar mismatch: got %v expected %v for test %v", s, test.s, test)
		}
	}
}

func TestConvertUnitErrors(t *testing.T) {
	tests := []struct {
		l, r string
		err  string
	}{
		{"s", "m", "no shortest path exists"},
		{"_XXX", "m", "Unknown left unit"},
		{"m", "_XXX", "Unknown right unit"},
	}
	for _, test := range tests {
		_, err := ConvertUnit(test.l, test.r)
		if err == nil {
			t.Errorf("Expected error")
			continue
		}
		s := fmt.Sprintf("%v", err)
		if !strings.Contains(s, test.err) {
			t.Errorf("Error test does not match: got [%v] and expected [%v] for test %v", s, test.err, test)
		}
	}
}
