# Introduction

sbrclang is unit-aware calculator written specifically for running. Using
sbrclang, a user can compute speed, distance, and time using a mix of units for
each dimension and solve trivial algrebra problems. It is modelled after the
awesome language [frink.](http://frinklang.org) If you need anything more
complicated then just use [frink.](http://frinklang.org)

# Language

The grammar is something like the following:

```
program    : expression ('->' expression ('@' expression)?)?
expression : factor ( op? factor )*
factor     : time
           | number
           | unit
           | '(' expression ')'
op         : '+' | '-' | '*' | '/'
number     : [0-9]+(.[0-9]+)?
unit       : [a-zA-Z]+
time       : (number ':')+
```

Each line consists of an expression, an optional conversion, and then an
optional iteration. An expression is sequence of terms and operators where terms
are numbers, time, or units and the supported operations are binary `+-*/`. Time
is represented as `hh:mm:ss.ssss`.

If the operator is not present, sbrclang will use an implicit operation. This
implicit operation places unit assignment at its highest priority (e.g. `18:00 5
km` will associate `km` to `5` before `18:00` to `5`) and then attempt a
reasonable operation if the operation is not a unit assignment. The reasonable
operation is:

- Add the terms if they have a single dimension and that dimension is the same
  (e.g. `3 hr 1 min` will add 3 hours to 1 minute.)

- Create a velocity if the terms are a distance and a time (e.g. `6:00 mi` will
  switch the terms and divide to make `mi / 6:00` and `mi 6:00` will just
  divide.)

- Otherwise multiply (e.g. `2 10 min` will yield 20 minutes.)

If there is only a single expession, sbrclang will evaulate that expression and
then return.

The result of this expression can be converted to another unit by using the
arrow operator `->` followed by another expression (e.g. `5 km -> mile`.) If one
expression is a velocity and the other is a single dimension, sbrclang will
solve for the missing dimension. For example, `18:00 5 km -> mi` will solve for
time and `6:00 mi -> 20:00` will solve for distance. This is same as `1 / (18:00
5 km / mi)` and `6:00 * 20 mi` but more readable.

The result of the conversion can then be iterated using the `@` operator and
then another expression (e.g. `18:00 5 km -> mi @ 5 km`.) The expression to the
left of the `@` is the step of the iteration and the right hand side is the
upper bound.

# Usage

There is a CLI named `sbrclang.go` which takes a program string as input. There
should also be a web interface on the [South Brooklyn Running
Club](http://southbrooklynrunning.com) website.

```shell
% go run sbrclang.go "7:30 mi * 2 hr"
16 mi
% go build sbrclang.go
% ./sbrclang "7:30 mi * 2 hr"
16 mi
```

# Examples

**My long run calls for 20mi @ 8:30 mi pace and the SBRC early group starts at 8am. Will I make it in time for my 11am brunch?**

```shell
% go run sbrclang.go "8:30 mi -> 20 mi"
2:50:00.00
```

**That's too close. Can I call it a long run if I just run for 2 hours?**

```shell
% go run sbrclang.go "8:30 mi * 2 hr"
14.12 mi
```


**I guess not. How long do I have to run at 8:30 mi pace to reach 15 miles?**

```shell
% go run sbrclang.go "8:30 mi -> 15 mi"
2:07:30.00
```

**I heard some guy brag about a thousand second 5K. Is that any good?**

```shell
% go run sbrclang.go "1000 s 5 km -> 5 km"
16:40.00
```

**What pace do I have to run to finish a 5K in 18 minutes?**

```shell
% go run sbrclang.go "18:00 5 km -> mi"
5:47.62
```

**I killed it on Miller Monday and can only muster a 7 minute mile pace. What time will I finish a 5K?**

```shell
% go run sbrclang.go "7:00 mi -> 5 km"
21:44.88
```

**I have a complicated workout from "Faster Road Racing". Help me out here.**

This workout calls for a 9 mi run that includes a VO2 max session consisting of
3 minutes at hard pace followed by 3 minutes easy pace. This is repeated 7
times. How many miles do I have to run in addition to the VO2 max workout?

```shell
% go run sbrclang.go "9 mi - 7 * (6:00 mi * 3 min + 9:00 mi * 3 min)"
3.17 mi
```

**Nice. Here's another.**

This workout calls for 2 sets of 5x200 at mile race pace. How much time I will I spend suffering?

```shell
% go run sbrclang.go "6:00 mi -> 2 * 5 * 200 m"
7:27.39
```

**I'm steady as a rock once I dial it in. What should the first 100m of a 6 minute 1600m be run in?**

```shell
% go run sbrclang.go "6:00 1600 m -> 100 m"
0:22.50
```

**I always wondered what a 6 minute mile is converted to 1600 m.**

```shell
% go run sbrclang.go "6:00 mi -> 1600 m"
5:57.91
```

**How fast (in miles per hour) was Bolt running when he set the world record in the 100m?**

```shell
% go run sbrclang.go "100 m / 9.58 s  -> mi / hour"
23.35 mi / hour
```

**I just ran a Prospect Park loop in 20 minutes. What time will I definitely get in the next Al Goldstien 5K?**

```shell
% go run sbrclang.go "20:00 parkloop -> 5 km"
18:49.77
```

**I just did a few strides, raced the Al Goldstien 5K, and then jogged it home. My watch screwed up, so how far was that?**

```shell
% go run sbrclang.go "4 strides 5 km 2 mi"
5.36 mi
```

**The 7am group wants to run a Loop de Bergen at a pace that will arrive in time for the 8am group. What pace should they run at?**

```shell
% go run sbrclang.go "1 hr loopdebergen -> mi"
8:34.29
```

**How many Carroll Park loops make a marathon?**

```shell
% go run sbrclang.go "marathon -> carrollparkloop"
94.62 carrollparkloop
```

# Example Charts

**What is the pace chart for an 18 minute 5K?**

```shell
% go run sbrclang.go "18:00 5 km -> mi @ 5km"
5:47.62

   01    5:47.62   1.00 mi
   02   11:35.24   2.00 mi
   03   17:22.85   3.00 mi
   04   18:00.00   3.11 mi
```

**What is the pace chart for a 5K run at 6 minute mile?**

```shell
% go run sbrclang.go "6:00 mi -> mi @ 5 km"
6:00.00

   01    6:00.00   1.00 mi
   02   12:00.00   2.00 mi
   03   18:00.00   3.00 mi
   04   18:38.47   3.11 mi
```

**What are the lap times for a 6 minute mile?**

```shell
% go run sbrclang.go "6:00 1600 m -> 400 m @ 1600 m"
1:30.00

   01   1:30.00    400.00 m
   02   3:00.00    800.00 m
   03   4:30.00   1200.00 m
   04   6:00.00   1600.00 m
```

**I'm running 7:30 mi pace for an hour, how far have I gone every 15 minutes?**

```shell
% go run sbrclang.go "7:30 mi -> 15 min @ hour"
2.00 mi

   01   2.00 mi   15:00.00
   02   4.00 mi   30:00.00
   03   6.00 mi   45:00.00
   04   8.00 mi   60:00.00
```

**How long have I run when doing 10x400m at 6 minute mi pace?**

```shell
% go run sbrclang.go "6:00 1600 m -> 400 m @ 10"
1:30.00

   01    1:30.00    1.00
   02    3:00.00    2.00
   03    4:30.00    3.00
...
   08   12:00.00    8.00
   09   13:30.00    9.00
   10   15:00.00   10.00
```

**That's nice, but what is the distance?**

```shell
% go run sbrclang.go "6:00 1600 m -> 400 m @ 10 * 400 m"
1:30.00

   01    1:30.00    400.00 m
   02    3:00.00    800.00 m
   03    4:30.00   1200.00 m
...
   08   12:00.00   3200.00 m
   09   13:30.00   3600.00 m
   10   15:00.00   4000.00 m
```

**I have to run 1 km at 5 km race pace. What are the lap times?**

```shell
% go run sbrclang.go "18:00 5 km -> 400 m @ 1000 m"
1:26.40

   01   1:26.40    400.00 m
   02   2:52.80    800.00 m
   03   3:36.00   1000.00 m
```

**What is the pace chart for a 3 hour marathon?**

```shell
% go run sbrclang.go "3 hr marathon -> mi @ marathon"
6:52.21

   01      6:52.21    1.00 mi
   02     13:44.43    2.00 mi
   03     20:36.64    3.00 mi
...
   25   2:51:45.34   25.00 mi
   26   2:58:37.56   26.00 mi
   27   3:00:00.00   26.20 mi
```

# About

sbrclang was written during a boring Maine vacation. The name is a tribute to
the classiest running club in southern Brooklyn, the [South Brooklyn Running
Club](http://southbrooklynrunning.com). Shoot us an email or check out our
schedule on the site to come run with us.

Props fly out to Flemming Andersen for the shortest path implementation at:

https://github.com/flemeur/go-shortestpath
